#!/bin/bash
# This script checks whether required TFC variables are set
# If all variables are successful, it writes a workspace_id file
rm -f vars_are_valid

# Check if curl and jq are installed
if ! [ -x "$(command -v jq)" ]; then
  echo "Error: jq is not installed. This script relies on curl and jq. Please install both and retry."
  exit 1
fi
if ! [ -x "$(command -v curl)" ]; then
  echo "Error: curl is not installed. This script relies on curl and jq. Please install both and retry."
  exit 1
fi

# Check if VAULT_TOKEN exists
if [ ! -z "$VAULT_TOKEN" ]; then
  echo "VAULT_TOKEN environment variable was found."
else
  echo "ERR: VAULT_TOKEN environment variable was not set."
  echo "You must export/set the VAULT_TOKEN environment variable."
  echo "It should be the root or an admin token"
  echo "Exiting."
  exit 1
fi

# Evaluate $VAULT_ADDR environment variable
# If not set, give error and exit
if [ ! -z "$VAULT_ADDR" ]; then
  echo "Using Vault Address: ${VAULT_ADDR}."
else
  echo "ERR: You must export/set the VAULT_ADDR environment variable."
  echo "Exiting."
  exit 1
fi

# Check if TFC_TOKEN exists
if [ ! -z "$TFC_TOKEN" ]; then
  echo "TFC_TOKEN environment variable was found."
else
  echo "ERR: TFC_TOKEN environment variable was not set."
  echo "You must export/set the TFC_TOKEN environment variable."
  echo "It should be a user or team token that has write or admin"
  echo "permission on the workspace."
  echo "Exiting."
  exit 1
fi

# Evaluate $GOOGLE_PROJECT environment variable
# If not set, give error and exit
if [ ! -z "$GOOGLE_PROJECT" ]; then
  echo "Using Google Project: ${GOOGLE_PROJECT}."
else
  echo "ERR: You must export/set the GOOGLE_PROJECT environment variable to the Project ID (usually the same as Project name)."
  echo "Exiting."
  exit 1
fi

# Evaluate $GOOGLE_CREDENTIALS_PATH environment variable
# If not set, give error and exit
if [ ! -z "$GOOGLE_CREDENTIALS_PATH" ]; then
  echo "Using Google Project: ${GOOGLE_CREDENTIALS_PATH}."
else
  echo "ERR: You must export/set the GOOGLE_CREDENTIALS_PATH environment variable."
  echo "The value of GOOGLE_CREDENTIALS_PATH should reflect the full path to your service account .json file."
  echo "Exiting."
  exit 1
fi

touch vars_are_valid
